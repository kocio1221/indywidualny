package com.kot.apteka.demo.tmp;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class MedicineTmp {


    @NotNull(message = "required field")
    private int medicineId;

    @NotNull(message = "required field")
    @Min(value = 1)
    @Max(value = 100000)
    private int price;


    @NotNull(message = "required field")
    private String medicineName;

    @NotNull(message = "required field")
    @Min(value = 1)
    @Max(value = 1000)
    private int medicineQuantity;

    @NotNull(message = "required field")
    @Min(value = 1)
    @Max(value = 1000000)
    private int medicineValue;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull(message = "Please provide a date.")
    private Date medicineDate;

    @NotNull(message = "required field")
    private int medicine_user_id;

    public MedicineTmp(){

    }

    public MedicineTmp(int medicineId, int price, int medicineQuantity, int medicineValue, Date medicineDate, int medicine_user_id) {
        this.medicineId = medicineId;
        this.price = price;
        this.medicineQuantity = medicineQuantity;
        this.medicineValue = medicineValue;
        this.medicineDate = medicineDate;
        this.medicine_user_id = medicine_user_id;
    }

    public int getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(int medicineId) {
        this.medicineId = medicineId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getMedicineQuantity() {
        return medicineQuantity;
    }

    public void setMedicineQuantity(int medicineQuantity) {
        this.medicineQuantity = medicineQuantity;
    }

    public int getMedicineValue() {
        return medicineValue;
    }

    public void setMedicineValue(int medicineValue) {
        this.medicineValue = medicineValue;
    }

    public Date getMedicineDate() {
        return medicineDate;
    }

    public void setMedicineDate(Date medicineDate) {
        this.medicineDate = medicineDate;
    }

    public int getMedicine_user_id() {
        return medicine_user_id;
    }

    public void setMedicine_user_id(int medicine_user_id) {
        this.medicine_user_id = medicine_user_id;
    }

    public int countPrice(int pricePerOne, int medicineQuantity){
        return pricePerOne*medicineQuantity;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }
}
