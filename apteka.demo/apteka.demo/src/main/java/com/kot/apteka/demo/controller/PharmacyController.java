package com.kot.apteka.demo.controller;


import com.kot.apteka.demo.entity.Medicine;
import com.kot.apteka.demo.service.MedicineService;
import com.kot.apteka.demo.tmp.MedicineTmp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@Controller
public class PharmacyController {


    private final MedicineService medicineService;

    @Autowired
    public PharmacyController(MedicineService medicineService) {
        this.medicineService = medicineService;
    }

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }

    @RequestMapping("/")
    public String homePage() {
        return "home";
    }


    // reservations of user
    @GetMapping("/user-medicines-view")
    public String reservationsList(Model model) {

        Collection<Medicine> medicines = medicineService.getAllMedicines();
        if(medicines!=null){
            // list of reservations for logged user
            model.addAttribute("medicineList", medicines);
            return "user-medicines-view";
        }

        return "redirect:/home";

    }

    // booking page
    @GetMapping("/new-medicines")
    public String newReservation(Model model) {
        // reservation attribute
        model.addAttribute("medicineTmp", new MedicineTmp());

        return "medicines-add";
    }

    // save new reservation
    @PostMapping("/proceed-medicine")
    public String proceedReservation(@Valid @ModelAttribute("currentMedicine") MedicineTmp medicineTmp) {

        //code for counting price
        /*int price = medicineTmp.countPrice(medicineTmp.getPrice(), medicineTmp.getMedicineQuantity());
        medicineTmp.setPrice(price);*/
        // send reservation to services to save it in database
        medicineTmp.setMedicineValue(10);
        medicineService.saveOrUpdateMedicine(medicineTmp);

        return "redirect:/user-medicines-view";
    }

    // delete reservation
    @PostMapping("/medicine-delete")
    public String deleteReservation(@RequestParam("medId") int medId) {

        medicineService.deleteMedicine(medId);
        return "redirect:/user-medicines-view";
    }





}
