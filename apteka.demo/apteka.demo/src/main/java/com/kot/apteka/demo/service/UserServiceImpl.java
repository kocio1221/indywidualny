package com.kot.apteka.demo.service;


import com.kot.apteka.demo.entity.Role;
import com.kot.apteka.demo.entity.User;
import com.kot.apteka.demo.repo.RoleRepository;
import com.kot.apteka.demo.repo.UserRepository;
import com.kot.apteka.demo.tmp.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
  //  private RoleDao roleDao;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository){
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
   //     this.roleDao = roleDao;
    }

    @Override
    @Transactional
    public User findUserByEmail(String email) {
        return userRepository.findUserByUserEmail(email);
    }

    @Override
    @Transactional
    public User findUserByUserName(String userName) {
        return userRepository.findByUserUsername(userName);
    }


    @Override
    @Transactional
    public void saveUser(CurrentUser currentUser) {
        User user = new User();

        user.setUserUsername(currentUser.getUsername());
        user.setUserPassword(currentUser.getPassword());
        user.setUserEmail(currentUser.getEmail());
        user.setUserRoles(Arrays.asList(roleRepository.findRoleByRoleName("ROLE_EMPLOYEE")));

        userRepository.save(user);
    }

    @Override
    @Transactional
    public int getLoggedUserId() {
        User user = userRepository.findByUserUsername(getLoggedEmail());
        if(user!=null){
            return user.getUserId();
        }
        return -1;
    }




    //Get the Current Logged-In user email using security user detail principal
    private String getLoggedEmail(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        }
        return principal.toString();
    }



    ///security login check valid username and role
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        User user = userRepository.findByUserUsername(userName);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getUserUsername(), user.getUserPassword(),
                mapRolesToAuthorities(user.getUserRoles()));
    }
    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        List<SimpleGrantedAuthority> authorities = roles.stream().map(role -> new SimpleGrantedAuthority(role.getRoleName())).collect(Collectors.toList());
        return authorities;
    }


    //spring security needs some password encoder bean, in this case im not encoding passwords
    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }
}
