package com.kot.apteka.demo.repo;


import com.kot.apteka.demo.entity.Medicine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MedicineRepository extends JpaRepository<Medicine,Integer> {

    Medicine findByMedicineId(int medicineId);

}
