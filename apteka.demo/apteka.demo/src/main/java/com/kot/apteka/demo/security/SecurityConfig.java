package com.kot.apteka.demo.security;

import com.kot.apteka.demo.entity.User;
import com.kot.apteka.demo.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpSession;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public final UserService userService;

    @Autowired
    public SecurityConfig(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/", "/user-reservations-view").hasRole("EMPLOYEE")
                .antMatchers("/new-reservation").hasRole("ADMIN")
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/authenticateTheUser")            /// this doesnt require any mapping, spring security does it automatically
                .successHandler((httpServletRequest, httpServletResponse, authentication) -> {       // creating new AuthenticationSuccessHandler with lambda
                    String email = authentication.getName();
                    User user = userService.findUserByEmail(email);
                    // now place in the session
                    HttpSession session = httpServletRequest.getSession();
                    session.setAttribute("user", user);
                    // forward to home page
                    httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/");
                })
                .permitAll()
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))   //
                .logoutSuccessUrl("/login")
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/access-denied");


    }


    //authentication, JPA does not have a out-of-the-box implementation
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService);
    }
}
