package com.kot.apteka.demo.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "medicine")
public class Medicine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "medicine_id")
    private int medicineId;

    @Column(name = "medicine_name")
    private String medicineName;

    @Column(name = "medicine_price")
    private int price;

    @Column(name = "medicine_quantity")
    private int medicineQuantity;

    @Column(name = "medicine_valiue")
    private int medicineValue;

    @Column(name = "medicine_add_date")
    private Date medicineDate;

    @Column(name = "medicine_user_id")
    private int medicineUserId;

    public Medicine() {
    }

    public Medicine(int medicineId, int price, int medicineQuantity, int medicineValue, int medicineUserId, String medicineName) {
        this.medicineName=medicineName;
        this.medicineId = medicineId;
        this.price = price;
        this.medicineQuantity = medicineQuantity;
        this.medicineValue = medicineValue;
        this.medicineUserId = medicineUserId;
    }



    public int getMedicineId() {
        return medicineId;
    }


    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getMedicineQuantity() {
        return medicineQuantity;
    }

    public void setMedicineQuantity(int medicineQuantity) {
        this.medicineQuantity = medicineQuantity;
    }

    public int getMedicineValue() {
        return medicineValue;
    }

    public void setMedicineValue(int medicineValue) {
        this.medicineValue = medicineValue;
    }

    public int getmedicineUserId() {
        return medicineUserId;
    }

    public void setmedicineUserId(int medicineUserId) {
        this.medicineUserId = medicineUserId;
    }

    public Date getMedicineDate() {
        return medicineDate;
    }

    public void setMedicineDate(Date medicineDate) {
        this.medicineDate = medicineDate;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    @Override
    public String toString() {
        return "Medicine{" +
                "medicineId=" + medicineId +
                ", price=" + price +
                ", medicineQuantity=" + medicineQuantity +
                ", medicineValue=" + medicineValue +
                ", medicineDate=" + medicineDate +
                ", medicineUserId=" + medicineUserId +
                '}';
    }
}


