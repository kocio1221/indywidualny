package com.kot.apteka.demo.repo;

import com.kot.apteka.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    User findUserByUserEmail(String email);
    User findByUserUsername(String username);
}
