package com.kot.apteka.demo.service;


import com.kot.apteka.demo.entity.User;
import com.kot.apteka.demo.tmp.CurrentUser;
import org.springframework.security.core.userdetails.UserDetailsService;


public interface UserService extends UserDetailsService {


    User findUserByEmail(String email);

    User findUserByUserName(String userName);

    void saveUser(CurrentUser currentUser);

    int getLoggedUserId();

}
