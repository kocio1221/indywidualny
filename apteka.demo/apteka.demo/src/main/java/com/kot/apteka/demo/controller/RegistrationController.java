package com.kot.apteka.demo.controller;


import com.kot.apteka.demo.entity.User;
import com.kot.apteka.demo.service.UserService;
import com.kot.apteka.demo.tmp.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.logging.Logger;

@Controller
@RequestMapping("/register")
public class RegistrationController {

	@Autowired
    private UserService userService;
	
    private Logger logger = Logger.getLogger(getClass().getName());


    //trims all the blank spaces for each string coming as part of request
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		
		StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
		dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);

	}	
	
	@GetMapping("/showRegistrationForm")
	public String showMyLoginPage(Model theModel) {
		
		theModel.addAttribute("currentUser", new CurrentUser());
		
		return "register";
	}

	@PostMapping("/processRegistrationForm")
	public String processRegistrationForm(
				@Valid @ModelAttribute("currentUser") CurrentUser currentUser,
				BindingResult theBindingResult, 
				Model theModel) {
		
		String username = currentUser.getUsername();
		logger.info("Processing registration form for: " + username);
		
		// form validation
		 if (theBindingResult.hasErrors()){
			 return "register";
		 }

		// check the database if user already exists
        User existing = userService.findUserByUserName(username);
        if (existing != null){
        	theModel.addAttribute("currentUser", new CurrentUser());
			theModel.addAttribute("registrationError", "User name already exists.");

			logger.warning("User name already exists.");
        	return "register";
        }
        
        // create user account        						
        userService.saveUser(currentUser);

        logger.info("Successfully created user: " + currentUser.getUsername());
		theModel.addAttribute("successRegistration", "Account successfully created");
        return "redirect:/login";
	}
}
