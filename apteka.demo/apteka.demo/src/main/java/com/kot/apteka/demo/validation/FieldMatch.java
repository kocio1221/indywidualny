package com.kot.apteka.demo.validation;


import javax.validation.Payload;
import java.lang.annotation.*;

public @interface FieldMatch {

    // custom annotation for finding matches between two string fields
    String message() default "";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    String first();
    String second();


    @Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List
    {
        FieldMatch[] value();
    }
}
