package com.kot.apteka.demo.controller;


import com.kot.apteka.demo.tmp.CurrentUser;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class LoginLogoutController {

    @GetMapping("/login")
    public String loginPage(Model model) {

        //The simplest way to retrieve the currently authenticated principal
        // if user is already login, redirect to home
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return "redirect:/";
        }

        model.addAttribute("newUser", new CurrentUser());
        return "login";
    }



}
