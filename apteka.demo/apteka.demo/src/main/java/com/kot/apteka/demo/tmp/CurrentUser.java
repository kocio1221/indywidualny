package com.kot.apteka.demo.tmp;



import com.kot.apteka.demo.validation.FieldMatch;
import com.kot.apteka.demo.validation.ValidEmail;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@FieldMatch.List({
        @FieldMatch(first = "password", second = "matchingPassword", message = "Password fields must match.")
})
public class CurrentUser {

    @NotNull(message = "required field")
    @Size(min = 3, max = 15)
    private String username;

    @NotNull(message = "required field")
    @Size(min = 3, max = 15)
    private String password;

    @NotNull(message = "is required")
    @Size(min = 3, max = 15)
    private String matchingPassword;

    @NotNull(message = "required field")
    @Size(min = 3, max = 50)
    @ValidEmail
    private String email;


    public CurrentUser() {
    }

    public CurrentUser(String username, String password, String matchingPassword, String email) {
        this.username = username;
        this.password = password;
        this.matchingPassword = matchingPassword;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "CurrentUser{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", matchingPassword='" + matchingPassword + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
