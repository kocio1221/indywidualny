package com.kot.apteka.demo.service;


import com.kot.apteka.demo.entity.Medicine;
import com.kot.apteka.demo.tmp.MedicineTmp;

import java.util.Collection;

public interface MedicineService {

    Medicine getMedicineById(int reservationId);

    void saveOrUpdateMedicine(MedicineTmp currentMedicine);

    void deleteMedicine(int medicineId);

    Collection<Medicine> getAllMedicines();

    MedicineTmp convertMedicineToMedicineTmp(int medicineId);

}
