package com.kot.apteka.demo.service;


import com.kot.apteka.demo.entity.Medicine;
import com.kot.apteka.demo.repo.MedicineRepository;
import com.kot.apteka.demo.tmp.MedicineTmp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;


@Service
public class MedicineServiceImpl implements MedicineService {

    private final UserService userService;
    private final MedicineRepository medicineRepository;

    @Autowired
    public MedicineServiceImpl(UserService userService, MedicineRepository medicineRepository){
        this.userService=userService;
        this.medicineRepository=medicineRepository;
    }

    @Override
    @Transactional
    public Medicine getMedicineById(int medicineId) {
        return medicineRepository.findByMedicineId(medicineId);
    }


    @Override
    @Transactional
    public Collection<Medicine> getAllMedicines(){
        return medicineRepository.findAll();
    }



    @Override
    @Transactional
    public void saveOrUpdateMedicine(MedicineTmp medicineTmp) {

        Medicine medicine = new Medicine();

        // get required id user using user services
        medicine.setmedicineUserId(userService.getLoggedUserId());

        medicine.setMedicineName(medicineTmp.getMedicineName());
        medicine.setPrice(medicineTmp.getPrice());
        medicine.setMedicineQuantity(medicineTmp.getMedicineQuantity());
        medicine.setMedicineDate(medicineTmp.getMedicineDate());
        medicine.setMedicineValue(medicineTmp.getPrice()*medicineTmp.getMedicineQuantity());
        medicineRepository.save(medicine);

    }

    @Override
    @Transactional
    public void deleteMedicine(int medicineId) {
        medicineRepository.deleteById(medicineId);
    }

    @Override
    public MedicineTmp convertMedicineToMedicineTmp(int medicineId) {
        Medicine medicine = getMedicineById(medicineId);
        MedicineTmp medicineTmp = new MedicineTmp();

        medicineTmp.setMedicineId(medicine.getMedicineId());
        medicineTmp.setMedicineName(medicine.getMedicineName());
        medicineTmp.setPrice(medicine.getPrice());
        medicineTmp.setMedicineDate(medicine.getMedicineDate());
        medicineTmp.setMedicineQuantity(medicine.getMedicineQuantity());
        medicineTmp.setMedicine_user_id(medicine.getmedicineUserId());
        medicineTmp.setMedicineValue(medicine.getMedicineValue());

        return medicineTmp;
    }


}