package com.kot.apteka.demo.dao;


import com.kot.apteka.demo.entity.Role;

public interface RoleDao {

	Role findRoleByName(String theRoleName);
}
